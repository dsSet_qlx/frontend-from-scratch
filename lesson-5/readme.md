## Ссылки

  - [Angular документация](https://angular.io/docs)
  - [Angular установка](https://angular.io/guide/setup-local)
  - [Angular tutorial](https://angular.io/tutorial/toh-pt0)
  - [Angular API](https://angular.io/api)
  - [RXJS](https://rxjs-dev.firebaseapp.com/guide/overview)
  - [RXJS tutorial](https://www.learnrxjs.io/)
   
## Домашнее задание

### реализовать приложение для просмотра валют 

 - Приложение должно включать в себя главный лейаут разделённый на две части
 - Слева должен выводиться список всех доступных валют. По клику на название валюты необходимо отображать страницу с информацией по выбранной валюте справа.
 - Переход по страницам вылют реализовать через роутинг
 - Страница валюты должна содержать краткую информацию о выбранной вадюте и простой конвертер валюты.
 - пример работы виджета можно посмотреть в [предыдущих материалах](https://bitbucket.org/dsSet_qlx/frontend-from-scratch/src/master/lesson-1/05-jquery/) (Это только пример работы виджета. Реализация должна быть написана полностью в парадигме Angular без использования jQuery и других библиотек!)
 - плюсом будет дополнительное оформление
    - поле с поиском по названию валюты
    - индикатор загрузки данных
    - вывод сообщений об ошибках 
 
### Подсказки
 - [Ссылка на документацию API](https://www.nbrb.by/apihelp/exrates).
 - для реализации потребуются методы `GET /exrates/currencies` и `GET /ExRates/Rates/${code}`
 - список всех валют и разметку структуры приложения нужно прописать в главном лейауте приложения
 - для построения url страниц использовать ISO код например `/currency/USD`
    
 
