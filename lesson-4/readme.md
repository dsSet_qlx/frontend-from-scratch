## Ссылки

  - [Angular документация](https://angular.io/docs)
  - [Angular установка](https://angular.io/guide/setup-local)
  - [Angular tutorial](https://angular.io/tutorial/toh-pt0)
  - [Angular API](https://angular.io/api)
  - [RXJS](https://rxjs-dev.firebaseapp.com/guide/overview)
  - [RXJS tutorial](https://www.learnrxjs.io/)
   
## Домашнее задание

 - реализовать TODO application
 - виджет должен быть реализован на angular
 - пример работы виджета можно посмотреть в [предыдущих материалах](https://bitbucket.org/dsSet_qlx/frontend-from-scratch/src/master/lesson-1/04-vanilla-js/)
 - (!) Это только пример работы виджета. Реализация должна быть написана полностью в парадигме Angular без использования jQuery и других библиотек!
 
### Подсказки
 - [Пример работы с инпутом (двухсторонний биндинг данных)](https://angular.io/guide/forms-overview#setup-in-template-driven-forms).

    
 
