
export class SystemOutput {

  constructor(public readonly message: string) {
  }

  public verbose(...args: Array<any>): void {
    console.log(this.message, ...args);
  }

  public error(error: Error): void {
    console.log(this.message);
    console.error(error);
  }

}
