// Destructuring assignment
function DestructuringAssignment() {
  const array = ['test', 1, { data: 'other stuff'}];

  const [value1, index1, data1] = array;
  
  const object = { value: 'test', index: 2, data: { test: '123' } };
  
  const { value, data, index } = object;
}

// Expression
function Expression({ a, b }: { a: number, b: number}) {
  return (
    <div>{a} * {b} = {a * b}</div>
  )
}


//Spread
function Spread(props) {
  return (
    <div {...props}>Render with delegated props</div>
  )

  // to replace className from props
  return (
    <div {...props} className="internal">Render with delegated props</div>
  )
}

/*
  <Spread className="test" role="button" />
  to be
  <div class="test" role="button">Render with delegated props</div>
*/


//Conditional rendering
function ConditionalRendering({ display }: { display: boolean }) {
  return (
    <div>
      {display && (<div>I'm visible for display === true</div>)}
      {display || (<div>I'm visible for display === false</div>)}
      {display ? (<div>value is true</div>) : (<div>value is false</div>)}
    </div>
  )
}

// ArrayRendering
function ArrayRendering() {
  return (
    <div>
      {[1, 2, 3].map(i => (<div key={i}>item {i}</div>))}
    </div>
  )
}


