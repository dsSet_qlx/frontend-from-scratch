

export interface TodoItemModel {
  id: number;
  value: string;
}
