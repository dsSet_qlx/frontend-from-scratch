import React, { useCallback, useState } from 'react';
import without from 'lodash/without';
import { TodoItemModel } from '../../models/TodoItemModel';
import { TodoList } from './TodoList';
import { ItemsGenerator } from './ItemsGenerator';

import './Todo.css';

interface ItemsManagerProps {
  defaultItems?: Array<TodoItemModel>;
}

export const ItemsManager: React.FC<ItemsManagerProps> = ({ defaultItems }) => {
  const [items, setItems] = useState(defaultItems || []);

  const handleAddItem = useCallback((item: TodoItemModel) => {
    setItems([...items, item]);
  }, [items, setItems]);

  const handleRemoveItem = useCallback((item: TodoItemModel) => {
    setItems(without(items, item));
  }, [items, setItems]);

  return (
    <section className='items-manager'>
      <ItemsGenerator onCreate={handleAddItem} />
      <TodoList items={items} onRemove={handleRemoveItem} />
    </section>
  );
};
