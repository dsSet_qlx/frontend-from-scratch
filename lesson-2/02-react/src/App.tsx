import React, { useEffect } from 'react';
import { ItemsManager } from './components/TodoList/ItemsManager';
import { TodoItemModel } from './models/TodoItemModel';

const defaults: Array<TodoItemModel> = [
  { id: 1000, value: 'default item' }
];

function App() {

  useEffect(() => {
    console.log('Application started');
  }, []);

  return (
    <ItemsManager defaultItems={defaults} />
  );
}

export default App;
