

(function (document, $) {

  function Api(apiUrl = 'https://www.nbrb.by/api') {
    this.getCurrenciesList = () => {
      return $.get(`${apiUrl}/exrates/currencies`).catch((e) => {
        console.error(e);
        return [];
      });
    };

    this.getCurrencyRate = (code) => {
      return $.get(`${apiUrl}/ExRates/Rates/${code}?ParamMode=2`);
    };

    return this;
  }


  function CurrenciesList(dropdownSelector, api) {
    const element = $(dropdownSelector);

    const createCurrencyOption = (currency) => {
      const $option = $('<option></option>');

      if (currency) {
        return $option.attr('value', currency.Cur_Abbreviation)
          .data('currency', currency)
          .text(`${currency.Cur_Abbreviation} | ${currency.Cur_Name}`)
      }

      return $option;
    };

    this.setCurrencies = (currencies) => {
      element.empty();
      const options = currencies.map(createCurrencyOption);

      element.append(createCurrencyOption(), options);
    };

    this.getSelectedCurrency = () => {
      const value = element.val();
      const option = $(`option[value=${value}]`, element);
      return option.first().data('currency');
    };

    this.onChange = null;

    element.on('change', () => {
      if (this.onChange) {
        this.onChange(element.val());
      }
    });

    api.getCurrenciesList().then(this.setCurrencies);

    return this;
  }


  function CurrenciesConverter(selector, api) {
    const $main = $(selector);
    const $input = $('.byn-value', $main);
    const $results = $('.result-container', $main);
    const $error = $('<div></div>')
      .css({ color: 'red', padding: '2rem' })
      .text('Could not load rate for selected currency')
      .hide();

    $main.append($error);

    let currency;
    let formatter;

    const updateValue = () => {
      let value = '';

      if (currency && $input.val() && formatter) {
        value = formatter.format(currency.Cur_Scale * $input.val() / currency.Cur_OfficialRate);
      }

      $('.value', $results).text(value || '');
    };

    const displayRate = (rateInfo) => {
      currency = rateInfo;
      formatter = new Intl.NumberFormat('ru-RU', { currency: currency.Cur_Abbreviation, currencyDisplay: 'symbol', style: 'currency' });
      $('.name', $results).text(`${currency.Cur_Abbreviation} | ${currency.Cur_Name}`);
      $('.rate', $results).text(`${currency.Cur_Scale} ${currency.Cur_Abbreviation} = ${currency.Cur_OfficialRate} BYN`);
      updateValue();
      $results.show();
    };

    const displayError = () => {
      $results.hide();
      $error.show();
    };

    this.clear = () => {
      currency = null;
      $results.hide();
    };

    this.getCurrency = () => {
      return currency;
    };

    this.setCurrency = (code) => {
      $error.hide();
      api.getCurrencyRate(code).then(displayRate).catch(displayError);
    };

    $input.on('input', updateValue);

    return this;
  }

  $(function () {
    const api = new Api();
    const $dropdown = $('#CurrenciesList', document);
    const $converter = $('#CurrenciesConverter', document);
    const currenciesList = new CurrenciesList($dropdown, api);
    const currenciesConverter = new CurrenciesConverter($converter, api);

    currenciesList.onChange = currenciesConverter.setCurrency;
  });

})(document, jQuery);
